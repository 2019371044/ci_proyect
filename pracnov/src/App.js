import './App.css';
import axios from 'axios';
import { Item } from './Item/Item';
import { useState, useEffect } from 'react';

function App() {

  const [tareas, setTareas] = useState([])

  useEffect(()=>{
    const obtenerTareas = async()=>{
      const url = 'api'
      const result = await axios.get(url)
      setTareas(result.data)
    }
    obtenerTareas()
  },[])

  return (
    <div className="App">
      <h1>Peticiones 2</h1>
      <ul>
        {
          tareas.map((tarea, i)=>{
            return(
              <Item
              data={tarea}
              key={tarea.id}
              />
            )
          })
        }
      </ul>
    </div>
  );
}

export default App;
