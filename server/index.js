const express = require('express');

const path = require('path');

const app = express();

const publicPath = path.join(__dirname, '../pracnov/build');
const port = process.env.PORT || 3001;


// Directorio Público
app.use(express.static(publicPath));

// Rutas 
const routes = require('./routes');
app.use('/api', routes );



app.listen(port, (err) => {

    if (err) throw new Error(err);

    console.log(`Servidor corriendo en puerto ${ port }`);

});
