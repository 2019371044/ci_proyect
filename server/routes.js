// Routes.js - Módulo de rutas
const express = require('express');
const router = express.Router();

const personasData = [
  {id:1, text:'Brayan'},
  {id:2, text:'Pue'},
  {id:3, text:'Adan'},
  {id:4, text:'Cris'},

]

// Get mensajes
router.get('/', function (req, res) {
  res.json(personasData);
});


module.exports = router;
